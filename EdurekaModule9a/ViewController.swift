//
//  ViewController.swift
//  EdurekaModule9a
//
//  Created by Jay on 02/03/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func setUserDefaultValue()
    {
        print("setUserDefaultValue started")
        
        let defaults = UserDefaults.standard
        let sampleString = "Hello Man. I just saved a string using user defaults"
        let key1 = "TestKey1"
        defaults.set(sampleString, forKey: key1)
        
        print("setUserDefaultValue ended")
    }

    @IBAction func getUserDefaultValue()
    {
        print("getUserDefaultValue started")
        
        let defaults = UserDefaults.standard
//        let sampleString = "Hello Man. I just saved a string using user defaults"
//        defaults.set(sampleString, forKey: "TestKey1")
        let key1 = "TestKey1"
        let sampleString = defaults.string(forKey: key1 )
        print("Value retrieved is this - " + sampleString!)
        print("getUserDefaultValue ended")
    }

}

